package hr.bbrdev.lastlogin.controller.request;

import lombok.Data;

@Data
public class LoginRequest {
    private String name;
}
