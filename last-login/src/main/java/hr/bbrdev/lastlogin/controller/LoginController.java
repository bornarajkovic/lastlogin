package hr.bbrdev.lastlogin.controller;

import hr.bbrdev.lastlogin.controller.request.LoginRequest;
import hr.bbrdev.lastlogin.controller.response.LoginResponse;
import hr.bbrdev.lastlogin.service.LoginService;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/last-login")
public class LoginController {

    private final LoginService loginService;

    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @PutMapping
    public LoginResponse updateLastLogin(@RequestBody LoginRequest loginRequest) {
        return new LoginResponse(loginService.updateLastLogin(loginRequest.getName()));
    }

    @GetMapping
    public LoginResponse getLastLogin(@RequestParam String name) {
        return new LoginResponse(loginService.getLastLogin(name));
    }
}
