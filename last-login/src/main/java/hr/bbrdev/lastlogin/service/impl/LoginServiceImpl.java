package hr.bbrdev.lastlogin.service.impl;

import hr.bbrdev.lastlogin.service.LoginService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Service
public class LoginServiceImpl implements LoginService {

    private final Map<String, LocalDateTime> lastLogins = new HashMap<>();

    @Override
    public LocalDateTime getLastLogin(String name) {
        return lastLogins.getOrDefault(name, null);
    }

    @Override
    public LocalDateTime updateLastLogin(String name) {
        return lastLogins.put(name, LocalDateTime.now());
    }
}
