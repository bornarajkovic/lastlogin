package hr.bbrdev.lastlogin.service;

import java.time.LocalDateTime;

public interface LoginService {
    LocalDateTime getLastLogin(String name);

    LocalDateTime updateLastLogin(String name);
}
