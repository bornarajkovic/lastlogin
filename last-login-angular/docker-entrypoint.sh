set -e

echo "Script start"

function error_exit
{
    echo "ERROR - $1 variable not defined"
    exit 1
}

if [[ -z $API ]]; then
    error_exit "API"
else
    echo "Building evn.json"
    echo "{\"api\": \""$API"\"}" > /etc/nginx/www/html/assets/env.json
fi
exec $@
