import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LoginService} from '../service/login.service';
import {Observable} from 'rxjs';
import {LoginResponse} from '../model/login-response';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public loginGroup: FormGroup;
  public sending = false;
  public time: string | null = null;

  constructor(private loginService: LoginService) {
    this.loginGroup = new FormGroup({
      name: new FormControl('', [Validators.required]),
      type: new FormControl('GET', [Validators.required])
    });
  }

  ngOnInit(): void {
  }

  public sendRequest(): void {
    if (this.loginGroup.valid) {
      this.sending = true;
      this.time = null;
      let observable: Observable<LoginResponse>;
      const value: any = this.loginGroup.value;
      if (value.type === 'GET') {
        observable = this.loginService.getLogin(value.name);
      } else {
        observable = this.loginService.updateLogin(value.name);
      }
      observable.subscribe(response => {
        this.sending = false;
        this.time = response.time;
      });
    } else {
      this.loginGroup.markAllAsTouched();
    }
  }
}
