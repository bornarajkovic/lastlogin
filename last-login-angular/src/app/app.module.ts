import {APP_INITIALIZER, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import {ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {HttpClientModule} from '@angular/common/http';
import {EnvironmentService} from './service/environment.service';

function EnvironmentLoader(environmentService: EnvironmentService): () => Promise<any> {
  return (): Promise<any> => {
    return environmentService.init();
  };
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatRadioModule,
    MatCardModule,
    MatButtonModule,
    HttpClientModule
  ],
  providers: [
    {provide: APP_INITIALIZER, useFactory: EnvironmentLoader, deps: [EnvironmentService], multi: true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
