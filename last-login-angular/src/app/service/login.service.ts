import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LoginResponse} from '../model/login-response';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpClient: HttpClient) { }

  public getLogin(name: string): Observable<LoginResponse> {
    return this.httpClient.get<LoginResponse>(environment.api + '/last-login', {params: {name}});
  }

  public updateLogin(name: string): Observable<LoginResponse> {
    return this.httpClient.put<LoginResponse>(environment.api + '/last-login', {name});
  }
}
