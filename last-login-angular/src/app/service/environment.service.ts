import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EnvironmentService {
  constructor() { }

  public init(): Promise<any> {
    return fetch('assets/env.json').then(value => value.json()).then(env => {
      environment.api = env.api;
    }).catch(() => console.log('Failed loading environment, using app defaults'));
  }
}
